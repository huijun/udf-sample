package org.itkk.udf.service.a.demo;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 描述 : UdfServiceADemoConfig
 *
 * @author wangkang
 */
@Configuration
public class UdfServiceADemoConfig {

    /**
     * 描述 : EXCHANGE_ADEMO_TEST1
     */
    public static final String EXCHANGE_ADEMO_TEST1 = "exchange.ademo.test1";

    /**
     * QUEUE_ADEMO_TEST1_CONSUME1
     */
    public static final String QUEUE_ADEMO_TEST1_CONSUME1 = "queue.ademo.test1.consume1";

    /**
     * 描述 : ROUTINGKEY_ADEMO_TEST1_TESTMSG
     */
    public static final String ROUTINGKEY_ADEMO_TEST1_TESTMSG = "routingkey.ademo.test1.testmsg";

    /**
     * exchangeAdemoTest1
     *
     * @return DirectExchange
     */
    @Bean
    public DirectExchange exchangeAdemoTest1() {
        return new DirectExchange(EXCHANGE_ADEMO_TEST1, true, true);
    }

    /**
     * queueAdemoTest1Consume1
     *
     * @return Queue
     */
    @Bean
    public Queue queueAdemoTest1Consume1() {
        return new Queue(QUEUE_ADEMO_TEST1_CONSUME1, true, false, true);
    }

    /**
     * queueAdemoTest1Consume1Binding
     *
     * @return Binding
     */
    @Bean
    public Binding queueAdemoTest1Consume1Binding() {
        return new Binding(QUEUE_ADEMO_TEST1_CONSUME1, Binding.DestinationType.QUEUE, EXCHANGE_ADEMO_TEST1, ROUTINGKEY_ADEMO_TEST1_TESTMSG, null);
    }

}
