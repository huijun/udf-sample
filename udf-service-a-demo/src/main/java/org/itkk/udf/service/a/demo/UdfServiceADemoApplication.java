package org.itkk.udf.service.a.demo;

import org.itkk.udf.core.BaseApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

/**
 * 描述 : 系统入口
 *
 * @author wangkang
 */
@SpringCloudApplication
public class UdfServiceADemoApplication extends BaseApplication {

    /**
     * 描述 : spring boot的入口，在整个项目中，包括其子项目在内， 只能有一个main方法，否则spring boot启动不起来
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(UdfServiceADemoApplication.class, args);
    }

}
