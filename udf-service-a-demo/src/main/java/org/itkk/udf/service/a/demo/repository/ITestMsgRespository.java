package org.itkk.udf.service.a.demo.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.itkk.udf.service.a.demo.domain.TestMsg;

/**
 * ITestMsgRespository
 */
@Mapper
public interface ITestMsgRespository {
    /**
     * count
     *
     * @param businessKey businessKey
     * @return int
     */
    int count(@Param("businessKey") String businessKey);

    /**
     * insert
     *
     * @param testMsg testMsg
     * @return int
     */
    int insert(TestMsg testMsg);

    /**
     * update
     *
     * @param testMsg testMsg
     * @return int
     */
    int update(TestMsg testMsg);
}
