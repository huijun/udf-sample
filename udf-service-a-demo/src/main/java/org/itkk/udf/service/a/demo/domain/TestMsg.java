package org.itkk.udf.service.a.demo.domain;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * TestMsg
 */
@ToString
@Data
public class TestMsg implements Serializable {
    /**
     * 描述 : id
     */
    private static final long serialVersionUID = 1L;

    /**
     * msgId
     */
    private String msgId = UUID.randomUUID().toString();

    /**
     * businessKey
     */
    private String businessKey;

    /**
     * version
     */
    private long version;

    /**
     * data
     */
    private String data;

    /**
     * lastUpdatedDate
     */
    private Date lastUpdatedDate;
}
