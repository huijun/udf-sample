package org.itkk.udf.service.a.demo.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.itkk.udf.core.RestResponse;
import org.itkk.udf.dal.mybatis.plugin.pagequery.Page;
import org.itkk.udf.service.a.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * 描述 : DemoController
 *
 * @author Administrator
 */
@RestController
@RequestMapping("demo")
@Api(value = "demo", consumes = "application/json", produces = "application/json", protocols = "http")
public class DemoController {

    /**
     * 描述 : demoService
     */
    @Autowired
    private DemoService demoService;

    /**
     * 描述 : a接口
     *
     * @param param 参数1
     * @return 结果
     */
    @ApiOperation(value = "add", notes = "add")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "param", value = "参数1", required = true, dataType = "int")
    })
    @RequestMapping(value = "add/{param}", method = RequestMethod.GET)
    public RestResponse<Integer> a(@PathVariable Integer param) {
        return new RestResponse<>(demoService.add(param));
    }

    /**
     * 描述 : b接口
     *
     * @param count 参数1
     * @return 结果
     */
    @ApiOperation(value = "b", notes = "add")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "path", name = "count", value = "数量", required = true, dataType = "int")
    })
    @RequestMapping(value = "b/{count}", method = RequestMethod.GET)
    public RestResponse<List<String>> b(@PathVariable Integer count) {
        return new RestResponse<>(demoService.b(count));
    }

    /**
     * 描述 : c接口
     *
     * @return 结果
     */
    @ApiOperation(value = "c", notes = "c")
    @RequestMapping(value = "c", method = RequestMethod.GET)
    public RestResponse<Page<Map<String, Object>>> c() {
        return new RestResponse<>(demoService.c("clusterQuartzScheduler"));
    }

}
