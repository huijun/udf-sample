package org.itkk.udf.service.a.demo.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * IQrtzSchedulerStateRepository
 */
@Mapper
public interface IQrtzSchedulerStateRepository { //NOSONAR
    /**
     * 查询所有
     *
     * @param schedName schedName
     * @return List<Map<String, Object>>
     */
    List<Map<String, Object>> selectAll(@Param("schedName") String schedName);
}
