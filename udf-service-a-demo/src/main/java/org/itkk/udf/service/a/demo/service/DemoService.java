/**
 * DemoService.java
 * Created at 2017-05-02
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.service.a.demo.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.itkk.udf.core.RestResponse;
import org.itkk.udf.dal.mybatis.plugin.pagequery.Page;
import org.itkk.udf.dal.mybatis.plugin.pagequery.PageInterceptor;
import org.itkk.udf.rms.Rms;
import org.itkk.udf.service.a.demo.repository.IQrtzSchedulerStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 描述 : DemoService
 *
 * @author Administrator
 */
@Service
public class DemoService {


    /**
     * rms
     */
    @Autowired
    private Rms rms;


    /**
     * qrtzSchedulerStateRepository
     */
    @Autowired
    private IQrtzSchedulerStateRepository qrtzSchedulerStateRepository;


    /**
     * c
     *
     * @param name name
     * @return List<Map<String, Object>>
     */
    @Cacheable(value = "test", key = "'ttt_' + #name")
    public Page<Map<String, Object>> c(String name) {
        PageInterceptor.startPage(1, 1);
        return PageInterceptor.getPageResult(qrtzSchedulerStateRepository.selectAll(name));
    }

    /**
     * b
     *
     * @param count count
     * @return List<String>
     */
    public List<String> b(Integer count) {
        Map<String, Object> uriVariables = new HashMap<>();
        uriVariables.put("count", count);
        return rms.call("ID_2", null, null, new ParameterizedTypeReference<RestResponse<List<String>>>() {
        }, uriVariables).getBody().getResult();
    }

    /**
     * 描述 : add
     *
     * @param a a
     * @return a
     */
    @HystrixCommand(fallbackMethod = "addFallback")
    public Integer add(Integer a) {
        return a + a;
    }

    /**
     * 描述 : addFallback
     *
     * @param a a
     * @return a
     */
    public Integer addFallback(Integer a) {
        return a * a;
    }


}
