package org.itkk.udf.demo.web;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * LearnTest
 */
@Slf4j
public class LearnTest {

    @Test
    public void jvm() throws ClassNotFoundException {
        //类加载器
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        System.out.println(classLoader);
        System.out.println(classLoader.getParent());
        System.out.println(classLoader.getParent().getParent()); //Bootstrap ClassLoader使用C语言实现的 , 找不到确定的返回父类的方式 , 就返回null

        //类的加载
        ClassLoader loader = LearnTest.class.getClassLoader();
        System.out.println(loader);
        //使用ClassLoader.loadClass()来加载类，不会执行初始化块
        loader.loadClass("org.itkk.udf.demo.web.Test1");
        //使用Class.forName()来加载类，默认会执行初始化块
        Class.forName("org.itkk.udf.demo.web.Test1");
        //使用Class.forName()来加载类，指定类加载器,初始化时不执行静态块
        Class.forName("org.itkk.udf.demo.web.Test1", false, loader);

        //自定义类加载器
        MyClassLoader myClassLoader = new MyClassLoader();
        myClassLoader.setRoot("D:/develop/temp");
        Class<?> testClass;
        try {
            testClass = myClassLoader.loadClass("org.itkk.udf.demo.web.Test2");
            Object object = testClass.newInstance();
            System.out.println(object.getClass().getClassLoader());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

}
