/**
 * DemoControllerTest.java
 * Created at 2017-05-20
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.web;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.itkk.udf.demo.service.DemoService;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * 描述 : DemoControllerTest
 *
 * @author Administrator
 *
 */
@RunWith(SpringRunner.class)
@WebMvcTest(DemoController.class)
public class DemoControllerTest {

    /**
     * 描述 : mvc
     */
    @Autowired
    private MockMvc mvc;

    /**
     * 描述 : demoService
     */
    @MockBean
    private DemoService demoService;

    /**
     * 描述 : testAdd
     * 
     * @throws Exception Exception
     *
     */
    @Test
    @Ignore
    public void testAdd() throws Exception { //NOSONAR

        final Integer a = 100;
        final Integer b = 100;
        final Integer assertResult = 0;
        final Integer mockAssertResult = 0;

        given(demoService.add(a, b)).willReturn(mockAssertResult);

        this.mvc.perform(get("/demo/add").param("a", a.toString()).param("b", b.toString()))
                        .andDo(print()).andExpect(status().isOk())
                        .andExpect(jsonPath("result").value(assertResult));

    }

}
