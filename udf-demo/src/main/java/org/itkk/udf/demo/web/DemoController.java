/**
 * DemoController.java
 * Created at 2017-04-25
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.web;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.itkk.udf.core.RestResponse;
import org.itkk.udf.core.exception.SystemRuntimeException;
import org.itkk.udf.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 描述 : DemoController
 *
 * @author Administrator
 */
@RestController
@RequestMapping("demo")
@Api(value = "demo", consumes = "application/json", produces = "application/json",
        protocols = "http")
public class DemoController {

    /**
     * 描述 : demoService
     */
    @Autowired
    private DemoService demoService;

    /**
     * 描述 : 测试捕获运行时异常
     *
     * @return msg
     */
    @ApiOperation(value = "测试捕获运行时异常", notes = "测试捕获运行时异常")
    @RequestMapping(value = "test/runtime/exception", method = RequestMethod.GET)
    public RestResponse<String> testRuntimeException() {
        try {
            testabc();
        } catch (Exception e) { //NOSONAR
            return new RestResponse<>(e.toString());
        }
        return new RestResponse<>();
    }

    /**
     * 描述 : testabc
     */
    private void testabc() {
        throw new SystemRuntimeException("ttt");
    }

    /**
     * 描述 : add
     *
     * @param a a
     * @param b b
     * @return a+b
     */
    @ApiOperation(value = "add", notes = "add")
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "query", name = "a", value = "a", required = true,
                    dataType = "int"),
            @ApiImplicitParam(paramType = "query", name = "b", value = "a", required = true,
                    dataType = "int")})
    @RequestMapping(value = "add", method = RequestMethod.GET)
    public RestResponse<Integer> add(Integer a, Integer b) {
        return new RestResponse<>(demoService.add(a, b));
    }

    /**
     * 描述 : 异步机制(等待)
     *
     * @return 结果
     * @throws InterruptedException InterruptedException
     * @throws ExecutionException   ExecutionException
     */
    @ApiOperation(value = "异步机制(等待)", notes = "异步机制(等待)")
    @RequestMapping(value = "async/wait", method = RequestMethod.GET)
    public RestResponse<String> asyncWait() throws InterruptedException, ExecutionException { //NOSONAR
        Future<String> msg = demoService.async();
        final long time = 10000;
        while (true) {
            if (msg.isDone()) {
                break;
            }
            Thread.sleep(time);
        }
        return new RestResponse<>(msg.get());
    }

    /**
     * 描述 : 异步机制(不等待)
     *
     * @return 结果
     * @throws InterruptedException InterruptedException
     */
    @ApiOperation(value = "异步机制(不等待)", notes = "异步机制(不等待)")
    @RequestMapping(value = "async", method = RequestMethod.GET)
    public RestResponse<String> async() throws InterruptedException {
        demoService.async();
        return new RestResponse<>();
    }

    /**
     * 描述 : 重试机制
     *
     * @return 结果
     */
    @ApiOperation(value = "重试机制", notes = "重试机制")
    @RequestMapping(value = "retry", method = RequestMethod.GET)
    public RestResponse<Integer> retry() {
        return new RestResponse<>(demoService.retry());
    }

    /**
     * 描述 : a接口
     *
     * @param param 参数1
     * @return 结果
     */
    @ApiOperation(value = "a接口", notes = "a接口")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "path", name = "param", value = "参数1",
            required = true, dataType = "string")})
    @RequestMapping(value = "a/{param}", method = RequestMethod.GET)
    public RestResponse<String> a(@PathVariable String param) {
        return new RestResponse<>(param);
    }

    /**
     * 描述 : b接口
     *
     * @return 结果
     */
    @ApiOperation(value = "b接口", notes = "b接口")
    @RequestMapping(value = "b", method = RequestMethod.GET)
    public RestResponse<Date> b() {
        return new RestResponse<>(new Date());
    }

    /**
     * 描述 : c接口
     *
     * @param aheader 头信息A
     * @return 结果
     */
    @ApiOperation(value = "c接口", notes = "c接口")
    @ApiImplicitParams({@ApiImplicitParam(paramType = "header", name = "aheader", value = "头信息A",
            required = true, dataType = "string")})
    @RequestMapping(value = "c", method = RequestMethod.GET)
    public RestResponse<String> c(@RequestHeader("aheader") String aheader) {
        return new RestResponse<>(aheader);
    }

    /**
     * 描述 : 调用b接口
     *
     * @return 结果
     */
    @ApiOperation(value = "调用b接口", notes = "调用b接口")
    @RequestMapping(value = "call/b", method = RequestMethod.GET)
    public RestResponse<Date> cellb() {
        return new RestResponse<>(this.demoService.callHttpsRest());
    }

}
