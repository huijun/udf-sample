/**
 * User.java
 * Created at 2017-05-05
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.domain;

import javax.persistence.*;
import java.io.Serializable;

/**
 * 描述 : User
 *
 * @author Administrator
 */
@Entity
public class User implements Serializable {

    /**
     * 描述 : ID
     */
    private static final long serialVersionUID = 1L;

    /**
     * 描述 : id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * 描述 : name
     */
    @Column(length = 100, nullable = false)
    private String name;

    /**
     * 描述 : age
     */
    @Column(length = 3, nullable = false)
    private Integer age;

    /**
     * 描述 : gender
     */
    @Column(length = 1, nullable = false)
    private Integer gender;

    /**
     * 描述 : 获取gender
     *
     * @return the gender
     */
    public Integer getGender() {
        return gender;
    }

    /**
     * 描述 : 设置gender
     *
     * @param gender the gender to set
     */
    public void setGender(Integer gender) {
        this.gender = gender;
    }

    /**
     * 描述 : 获取id
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * 描述 : 设置id
     *
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 描述 : 获取name
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * 描述 : 设置name
     *
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 描述 : 获取age
     *
     * @return the age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * 描述 : 设置age
     *
     * @param age the age to set
     */
    public void setAge(Integer age) {
        this.age = age;
    }

}
