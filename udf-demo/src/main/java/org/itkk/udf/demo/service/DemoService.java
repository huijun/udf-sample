/**
 * DemoService.java
 * Created at 2017-05-02
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.service;

import org.itkk.udf.core.RestResponse;
import org.itkk.udf.core.exception.SystemRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.RetryException;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.security.SecureRandom;
import java.time.LocalTime;
import java.util.Date;
import java.util.concurrent.Future;

/**
 * 描述 : DemoService
 *
 * @author Administrator
 */
@Service
public class DemoService {

    /**
     * 描述 : 日志
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(DemoService.class);

    /**
     * 描述 : restTemplate
     */
    @Autowired
    @Qualifier("externalRestTemplate")
    private RestTemplate restTemplate;

    /**
     * 描述 : 加法
     *
     * @param a a
     * @param b b
     * @return a+b
     */
    public Integer add(Integer a, Integer b) {
        return a + b;
    }

    /**
     * 描述 : callHttpsRest
     *
     * @return date
     */
    public Date callHttpsRest() {
        String url = "https://wangkang.com:8443/demo/b"; //NOSONAR
        ResponseEntity<RestResponse<Date>> result = restTemplate.exchange(url, HttpMethod.GET, null,
                new ParameterizedTypeReference<RestResponse<Date>>() {
                });
        return result.getBody().getResult();
    }

    /**
     * 描述 : 异步机制
     *
     * @return msg
     * @throws InterruptedException InterruptedException
     */
    @Async
    public Future<String> async() throws InterruptedException {
        final long time = 5000;
        Thread.sleep(time);
        LOGGER.info("async exec");
        return new AsyncResult<>("job done");
    }

    /**
     * 描述 : 计划任务
     */
    @Scheduled(fixedRate = 20000)
    public void reportCurrentTime() {
        try {
            this.async();
        } catch (InterruptedException e) { //NOSONAR
            throw new SystemRuntimeException(e);
        }
        LOGGER.info("reportCurrentTime The time is now {}", LocalTime.now());
    }

    /**
     * 描述 : 测试重试
     *
     * @return num
     */
    @Retryable(Exception.class)
    public Integer retry() {
        LOGGER.info("测试retry");
        final int a = 5;
        int num = new SecureRandom().nextInt();
        if (num % a == 0) {
            return num;
        }
        throw new RetryException("重试失败");
    }

    /**
     * 描述 : 重试失败后执行的方法
     *
     * @param e e
     * @return e
     */
    @Recover
    public Integer recover(RetryException e) {
        throw e;
    }

}
