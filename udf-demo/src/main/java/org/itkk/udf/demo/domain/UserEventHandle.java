/**
 * UserEventHandle.java
 * Created at 2017-05-05
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.domain;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.core.annotation.HandleAfterSave;
import org.springframework.data.rest.core.annotation.HandleBeforeCreate;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

/**
 * 描述 : UserEventHandle
 *
 * @author Administrator
 */
@Component
@RepositoryEventHandler
public class UserEventHandle {

    /**
     * 描述 : 日志
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(UserEventHandle.class);

    /**
     * 描述 : HandleBeforeSave
     *
     * @param user user
     */
    @HandleBeforeSave
    public void handleBeforeSave(User user) {
        LOGGER.info("User handleBeforeSave : {}", user);
    }

    /**
     * 描述 : handleBeforeCreate
     *
     * @param user user
     */
    @HandleBeforeCreate
    public void handleBeforeCreate(User user) {
        LOGGER.info("User handleBeforeCreate : {}", user);
    }

    /**
     * 描述 : handleAfterSave
     *
     * @param user user
     */
    @HandleAfterSave
    public void handleAfterSave(User user) {
        LOGGER.info("User handleAfterSave : {}", user);
    }

}
