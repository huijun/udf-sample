/**
 * TestSch.java
 * Created at 2017-06-11
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.sch;

import java.util.Map;

import org.itkk.udf.scheduler.client.SchException;
import org.itkk.udf.scheduler.client.executor.AbstractExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * 描述 : TestSch
 *
 * @author Administrator
 */
@Component("testBean")
public class TestSch extends AbstractExecutor {

    /**
     * 描述 : 日志
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(TestSch.class);

    /**
     * 描述 : om
     */
    @Autowired
    private ObjectMapper om;

    @Override
    protected void handle(String id, Map<String, Object> jobDataMap) {
        try {
            LOGGER.info("任务执行了------id:{}, jobDataMap:{}", id, om.writeValueAsString(jobDataMap)); //NOSONAR
        } catch (JsonProcessingException e) {
            throw new SchException(e);
        }
    }

}
