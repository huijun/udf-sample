/**
 * IUserRepository.java
 * Created at 2017-05-05
 * Created by Administrator
 * Copyright (C) 2016 itkk.org, All rights reserved.
 */
package org.itkk.udf.demo.domain;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * 描述 : IUserRepository
 *
 * @author Administrator
 */
@RepositoryRestResource(path = "user")
public interface IUserRepository extends JpaRepository<User, Long> { //NOSONAR

    /**
     * 描述 : findByNameContainingIgnoreCase
     *
     * @param name name
     * @param p    p
     * @return Page<User>
     */
    Page<User> findByNameContainingIgnoreCase(@Param("name") String name, Pageable p);

}
