#!/usr/bin/env bash

echo 'stop application begin'
  PID=$(ps -ef | grep ${project.name}-${project.version}.jar | grep -v grep | awk '{ print $2 }')
  if [ -z "$PID" ]
  then
      echo 'application is already stopped'
  else
      echo kill $PID
      kill -9 $PID
  fi
  rm -rf /udf_app/${project.name}-${project.version}.jar
echo 'stop application end'

echo 'clean copy application begin'
rm -rf /udf_app/${project.name}-1.0.jar
cp ../${project.name}-1.0.jar /udf_app/${project.name}-${project.version}.jar
echo 'copy application end'

echo 'start application begin'
nohup java -server -Xms512m -Xmx512m -XX:+UseG1GC -jar /udf_app/${project.name}-${project.version}.jar --spring.profiles.active=qa &
echo 'start application end'